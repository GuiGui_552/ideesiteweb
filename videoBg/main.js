let clairSombre = document.querySelector('.clairSombre');
let menu = document.querySelector('.menu');
let body = document.querySelector('body');
let navigation = document.querySelector('.navigation');

clairSombre.onclick = function(){
    body.classList.toggle('dark');
    clairSombre.classList.toggle('active');
}

menu.onclick = function(){
    menu.classList.toggle('active');
    navigation.classList.toggle('active');
}